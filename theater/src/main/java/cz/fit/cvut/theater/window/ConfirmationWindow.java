package cz.fit.cvut.theater.window;

import com.vaadin.ui.Button;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;

public class ConfirmationWindow extends Window {
	
	private VerticalLayout content;
	VerticalLayout mainLayout;
	HorizontalLayout footer;

	
	public ConfirmationWindow(onConfirmEvent event) 
	{
		center();
		setResizable(false);
		setWidth("300px");
		setHeight("200px");
		setModal(true);
		
		content = new VerticalLayout();
		content.setSizeFull();
		content.setMargin(true);
		content.setSpacing(true);
		setContent(content);
		
		mainLayout = new VerticalLayout();
		mainLayout.setSpacing(true);
		content.addComponent(mainLayout);
		content.setExpandRatio(mainLayout, 2);
		
		footer = new HorizontalLayout();
		footer.setSpacing(true);
		content.addComponent(footer);
		
		Label l = new Label("Opuštěním tohoto okna ztratíte veškerý dosavadní postup v registraci. Jste si jistí?");
		l.setWidth("100%");
		mainLayout.addComponent(l);
		
		Button yesButton = new Button("Ano");
		yesButton.addClickListener(e -> {event.onConfirm(); close();});
		footer.addComponent(yesButton);
		
		Button noButton = new Button("Ne");
		noButton.addClickListener(e -> close());
		footer.addComponent(noButton);
		
	}
	
	public interface onConfirmEvent
	{
		public void onConfirm();
	}

}
