package cz.fit.cvut.theater.layout;

import java.util.ArrayList;
import java.util.List;

import com.vaadin.data.Item;
import com.vaadin.event.ItemClickEvent;
import com.vaadin.event.ItemClickEvent.ItemClickListener;
import com.vaadin.shared.ui.MarginInfo;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Table;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;

import cz.fit.cvut.theater.data.Database;
import cz.fit.cvut.theater.data.Play;
import cz.fit.cvut.theater.layout.shows.AbstractComboBox;
import cz.fit.cvut.theater.layout.shows.GenereComboBox;
import cz.fit.cvut.theater.layout.shows.MonthComboBox;
import cz.fit.cvut.theater.layout.shows.PlayComboBox;
import cz.fit.cvut.theater.layout.shows.ShowTable;
import cz.fit.cvut.theater.window.ShowDetailWindow;

public class ShowsLayout extends VerticalLayout {
	
	HorizontalLayout filtersLayout;
	Database db;
	
	private List<AbstractComboBox> filters;
	
	private ShowTable table;
	
	public ShowsLayout() 
	{
		db = new Database();
		
		setSpacing(true);
		setMargin(new MarginInfo(true, false, true, false));
		
		filtersLayout = new HorizontalLayout();
		filtersLayout.setSpacing(true);
		filtersLayout.setWidth("100%");
		
		filters = new ArrayList<AbstractComboBox>();
		filters.add(new MonthComboBox(this));
		filters.add(new GenereComboBox(this));
		filters.add(new PlayComboBox(this));
		for (AbstractComboBox c : filters)
		{
			c.setWidth("100%");
			filtersLayout.addComponent(c);
			filtersLayout.setExpandRatio(c, 1);
		}
		
		
		addComponent(filtersLayout);
		table = new ShowTable(db.getAllPlays());
		addComponent(table);
	}
	
	public void setTable(ShowTable table, AbstractComboBox comboBox)
	{
		removeComponent(this.table);
		addComponent(table);
		this.table = table;
		
		for (AbstractComboBox c : filters)
		{
			if (c != comboBox)
			{
				c.selectDefault();
			}
		}
		
	}
}
