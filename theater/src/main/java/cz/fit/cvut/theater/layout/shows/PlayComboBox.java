package cz.fit.cvut.theater.layout.shows;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import cz.fit.cvut.theater.data.Play;
import cz.fit.cvut.theater.layout.ShowsLayout;

public class PlayComboBox extends AbstractComboBox {
	
	public PlayComboBox(ShowsLayout showsLayout) {
		super(showsLayout);

		setItemCaption(defaul, "Všechny hry");
		
		HashSet<String> hashSet = new HashSet<String>();
		for (Play p : db.getAllPlays())
		{
			hashSet.add(p.basicInfo);
		}
		
		ArrayList<String> playNames = new ArrayList<String>(hashSet);
		
		for (String m : playNames)
		{
			addItem(m);
		}		
	}
	
	protected void onSelectionChanged()
	{
		System.out.println(getValue());

		if (getValue() instanceof String)
		{
			String selected = (String) getValue();
			
			List<Play> filteredPlays = new ArrayList<Play>();
			for (Play p : db.getAllPlays())
			{
				if (p.basicInfo.equals(selected))
				{
					filteredPlays.add(p);
				}
			}
			showsLayout.setTable(new ShowTable(filteredPlays), this);
		}
		else if (getValue() instanceof DefaultItem)
		{
			showsLayout.setTable(new ShowTable(db.getAllPlays()), this);
		}
	}
}
