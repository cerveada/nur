package cz.fit.cvut.theater;

import com.vaadin.server.Resource;
import com.vaadin.server.ThemeResource;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Image;
import com.vaadin.ui.Label;
import com.vaadin.ui.Notification;
import com.vaadin.ui.TabSheet;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;

import cz.fit.cvut.theater.data.Database;
import cz.fit.cvut.theater.layout.AboutLayout;
import cz.fit.cvut.theater.layout.ActorsLayout;
import cz.fit.cvut.theater.layout.ContactLayout;
import cz.fit.cvut.theater.layout.ShowsLayout;
import cz.fit.cvut.theater.window.ShowDetailWindow;

public class MainLayout extends VerticalLayout {
	
	public MainLayout() {
		
		setWidth("800px");
		
		
		HorizontalLayout header = new HorizontalLayout();
		header.setWidth("100%");
		
		Resource res = new ThemeResource("img/logo.png");
		Image logo = new Image(null, res);
		logo.setWidth("400px");
		logo.addStyleName("clickable");
		header.addComponent(logo);
		
		
		HorizontalLayout nextShowLayout = new HorizontalLayout();
		nextShowLayout.setSizeUndefined();
		nextShowLayout.addStyleName("next-show");
		nextShowLayout.addStyleName("clickable");
		nextShowLayout.addLayoutClickListener( e -> showNextPlayDetails() );
		
		Label time = new Label("18:00");
		time.setSizeUndefined();
		time.addStyleName("next-show-time");
		nextShowLayout.addComponent(time);
		
		Label nextShow = new Label(" Naši furianti");
		nextShow.setSizeUndefined();
		nextShowLayout.addComponent(nextShow);
		
		
		header.addComponent(nextShowLayout);
		header.setComponentAlignment(nextShowLayout, Alignment.TOP_RIGHT);

		
		addComponent(header);
		
		TabSheet sample = new TabSheet();
		sample.addStyleName("padded-tabbar");
		
		sample.addTab(new AboutLayout(), "O nás");
		sample.addTab(new ActorsLayout(), "Herci");
		sample.addTab(new ShowsLayout(), "Program");
		sample.addTab(new ContactLayout(), "Kontakt");
		
		logo.addClickListener( e -> sample.setSelectedTab(0) );
		
		addComponent(sample);
	}
	
	private void showNextPlayDetails()
	{
		ShowDetailWindow showDetailWindow = 
				new ShowDetailWindow((new Database()).getPlay(0));
		UI.getCurrent().addWindow(showDetailWindow);
	}
}
