package cz.fit.cvut.theater.layout.shows;

import com.vaadin.ui.ComboBox;

import cz.fit.cvut.theater.data.Database;
import cz.fit.cvut.theater.layout.ShowsLayout;

public class AbstractComboBox extends ComboBox {
	
	protected ShowsLayout showsLayout;
	protected Database db;
	protected DefaultItem defaul;
	
	private boolean durringReset;
	
	public AbstractComboBox(ShowsLayout showsLayout) {
		
		this.showsLayout = showsLayout;
		db = new Database();
		
		setNullSelectionAllowed(false);
		
		defaul = new DefaultItem();
		addItem(defaul);
		setValue(defaul);

		addValueChangeListener(e -> 
		{
			if (durringReset)
			{
				return;
			}
			onSelectionChanged();
		});
	}
	
	protected void onSelectionChanged() {}
	
	protected class DefaultItem {}

	public void selectDefault() {
		durringReset = true;
		setValue(defaul);
		durringReset = false;
	}	
}
