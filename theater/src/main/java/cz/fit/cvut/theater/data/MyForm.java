package cz.fit.cvut.theater.data;

import com.vaadin.data.Item;
import com.vaadin.data.Validator;
import com.vaadin.data.fieldgroup.FieldGroup;
import com.vaadin.data.validator.EmailValidator;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.NativeSelect;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.TextField;



public class MyForm extends FormLayout {

	
	public NativeSelect numberOfTickets = new NativeSelect("Počet lístků");
	public TextField firstname = new TextField("Křestní jméno");
    public TextField surname = new TextField("Příjmení");
    public TextField email = new TextField("E-mail");
    public TextField mobile = new TextField("Mobil");
    TextArea textArea = new TextArea("Obchodní podmínky");
    private String s = new String ("Souhlasím s Obchodními podmínkami");
    public CheckBox checkBox = new CheckBox(s);


    public MyForm(Item item) {
    	setImmediate(true);
    	
    	// Add the fields
    	
    	 for (int i = 1; i < 11; i++) {
    		 numberOfTickets.addItem(i);
    		 numberOfTickets.setItemCaption(i, "" + i);
    	 }
    	addComponent(numberOfTickets);
    	numberOfTickets.setRequired(true);
    	numberOfTickets.select(1);
    	
    	addComponent(firstname);
        firstname.setRequired(true);
        firstname.setRequiredError("Vyplňte prosím křestní jméno");
        firstname.setImmediate(true);
        
        addComponent(surname);
        surname.setRequired(true);
        surname.setRequiredError("Vyplňte prosím přijmení");
        surname.setImmediate(true);
        
        addComponent(email);
        email.setRequired(true);
        email.setRequiredError("Vyplňte prosím e-mailovou adresu");
        email.addValidator(new EmailValidator("Neplatný formát e-mailové adresy"));
        email.setImmediate(true);
        
        addComponent(mobile);

        textArea.setValue(getGangstaLoremIpsum());
        textArea.setEnabled(false);
        textArea.setSizeFull();
        addComponent(textArea);
        
        addComponent(checkBox);
        checkBox.setRequired(true);
        checkBox.addValidator(new CheckBoxValidator());
        checkBox.setValue(true);
        
        // Now bind the member fields to the item
        FieldGroup binder = new FieldGroup(item);
        binder.bindMemberFields(this);                
    }   
    
	public void validate() 
	{
		firstname.validate();
		surname.validate();
    	email.validate();
    	checkBox.validate();
	}

    private String getGangstaLoremIpsum()
    {
    	return "Lorizzle ipsum dolor mofo bow wow wow, fizzle adipiscing elit. "
    			+ "Bizzle sapizzle velit, aliquet volutpat, suscipit quizzle, "
    			+ "yo vizzle, dizzle. Pellentesque gangster tortizzle. Sizzle "
    			+ "yippiyo. Ass izzle dolizzle check it out turpis tempizzle "
    			+ "fizzle. Maurizzle pellentesque away et dizzle. You son of a"
    			+ " bizzle in tortizzle. Pellentesque shizzle my nizzle crocodizz"
    			+ "le pizzle sheezy. In ma nizzle habitasse fizzle dictumst. Stuff "
    			+ "dapibizzle. Get down get down mah nizzle bizzle, bizzle eu, you "
    			+ "son of a bizzle ac, eleifend vitae, nunc. Dang suscipizzle. Fo "
    			+ "shizzle my nizzle crackalackin velit sed daahng dawg.";
    }
    
    private class CheckBoxValidator implements Validator 
    {
    	
        @Override
        public void validate(Object value) throws InvalidValueException 
        {
            if ((boolean)value != true)
                throw new InvalidValueException("Musíte souhlasit s obchodními podmínkami");
        }
    }


}