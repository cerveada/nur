package cz.fit.cvut.theater.layout;

import com.vaadin.shared.ui.MarginInfo;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.VerticalLayout;

public class AboutLayout extends VerticalLayout {
	public AboutLayout(){
		setSpacing(true);
		setMargin(new MarginInfo(true, false, true, false));
		addComponent(welcomeLabel());
		addComponent(basicInfoLabel());
		addComponent(loremIpsumLabel());

	}
	
	private Label welcomeLabel(){
		Label label = new Label(
			    "Vítejte na stránkách Národního temelínského divadla o.p.s");
		return label;
	}
	
	private Label basicInfoLabel(){
		Label label = new Label("Národní temelínské divadlo se nachází v centru města Temelín \n a nabízí víceúčelové využití například pro divadelní představení, koncerty, plesy, taneční kurzy, semináře, prezentační akce, zkoušky a představení ochotnických divadelních souborů apod. Divadlo je možné si pronajmout pouze na 1 hodinu, stejně tak na několik dní. V sále je instalovaná kvalitní zvuková i světelná technika. Kapacita sálu je 280 míst, při plesové úpravě 146 míst a pro semináře a školení 200 míst. Při přípravě I realizaci Vašich akcí je Vám k dispozici personál divadla, problémem nejsou ani doprovodné služby jako catering, ozvučení a osvětlení či security. Pro více informací se na nás neváhejte obrátit.");
		return label;
	}
	
	
	private Label loremIpsumLabel(){
		Label label = new Label("Lorizzle ipsum sizzle sit pimpin', consectetizzle adipiscing elit. Nullizzle uhuh ... yih! velizzle, dang volutpat, stuff quis, mah nizzle vizzle, mammasay mammasa mamma oo sa. Dope away we gonna chung. Sed eros. Mofo hizzle dolor break yo neck, yall turpizzle tempizzle things. Maurizzle fo shizzle pimpin' i saw beyonces tizzles and my pizzle went crizzle turpizzle. Cool izzle for sure. Pellentesque sure rhoncizzle shiznit. In hizzle pimpin' platea dictumst.");
		return label;
	}	
}
