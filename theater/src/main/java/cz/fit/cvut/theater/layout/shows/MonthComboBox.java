package cz.fit.cvut.theater.layout.shows;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import cz.fit.cvut.theater.data.Play;
import cz.fit.cvut.theater.data.enumeration.Month;
import cz.fit.cvut.theater.layout.ShowsLayout;

public class MonthComboBox extends AbstractComboBox {
	
	public MonthComboBox(ShowsLayout showsLayout) {
		super(showsLayout);

		pageLength = 13;
		
		setItemCaption(defaul, "Všechny měsíce");
		
		for (Month m : Month.values())
		{
			addItem(m);
			setItemCaption(m, m.getName());
		}
		
		//addValueChangeListener(e -> onSelectionChanged());
	}
	
	protected void onSelectionChanged()
	{
		System.out.println(getValue());
		if (getValue() instanceof Month)
		{
			Month selected = (Month) getValue();
			
			List<Play> filteredPlays = new ArrayList<Play>();
			for (Play p : db.getAllPlays())
			{
				if (p.date.get(Calendar.MONTH) == selected.ordinal())
				{
					filteredPlays.add(p);
				}
			}
			showsLayout.setTable(new ShowTable(filteredPlays), this);
		}
		else if (getValue() instanceof DefaultItem)
		{
			showsLayout.setTable(new ShowTable(db.getAllPlays()), this);
		}
	}
}
