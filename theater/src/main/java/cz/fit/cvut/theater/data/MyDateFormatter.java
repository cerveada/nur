package cz.fit.cvut.theater.data;

import java.util.Calendar;
import java.util.GregorianCalendar;



public class MyDateFormatter
{
   public static String GetDateAndTime(GregorianCalendar date)
   {
	   String ret = GetOnlyDate(date)+" "+GetOnlyTime(date); 
	   return ret;
   }
   
   public static String GetOnlyDate(GregorianCalendar date){
      int day, month, year;
    
      day = date.get(Calendar.DAY_OF_MONTH);
      month = date.get(Calendar.MONTH);
      year = date.get(Calendar.YEAR);
      
      String ret = day+"."+(month+1)+"."+year;
      return ret;
   }
   public static String GetOnlyTime(GregorianCalendar date){
      int minute, hour;
      minute = date.get(Calendar.MINUTE);
      hour = date.get(Calendar.HOUR_OF_DAY);
      if (minute == 0){
          String ret = hour+":"+"00";
          return ret;
      }
      String ret = hour+":"+minute;
      return ret;
   }
   
}