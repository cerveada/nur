package cz.fit.cvut.theater.window;

import com.vaadin.ui.Button;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;

import cz.fit.cvut.theater.data.MyDateFormatter;
import cz.fit.cvut.theater.data.MyForm;
import cz.fit.cvut.theater.data.Play;

public class SecondRezervationWindow extends AbstractWindow {

	boolean readyToClose;
	
	public SecondRezervationWindow(MyForm form, Play play) 
	{
		setCaption("Rezervace vstupenek (2/2) - potvrzení zadaných údajů");
		basicPlayInfo(play);

		showPrice(form, play);
		
		form.setEnabled(false); // disable editing 
		mainLayout.addComponent(form);
		
		initFooter(form, play);
		
		addCloseListener(new RezervationCloseListener());
	}


	private void initFooter(MyForm form, Play play)
	{
		Button rezervationButton = new Button("Dokončit rezervaci");
		rezervationButton.addClickListener(e ->
		{
			UI.getCurrent().addWindow(new ThirdRezervationWindow(form, play));
			readyToClose = true;
			close();
		});
		footer.addComponent(rezervationButton);
		
		Button backButton = new Button("Upravit údaje");
		backButton.addClickListener(e ->
		{
			UI.getCurrent().addWindow(new FirstRezervationWindow(play, form));
			readyToClose = true;
			close();
		});
		footer.addComponent(backButton);
		
	}
	
	private void basicPlayInfo(Play play){
		HorizontalLayout horizontalLayout = new HorizontalLayout();
		String date = MyDateFormatter.GetDateAndTime(play.date);
		horizontalLayout.addComponent(new Label(date));
		horizontalLayout.addComponent(new Label(", "+play.basicInfo+" ("+play.author+")"));
		horizontalLayout.addComponent(new Label(", Cena: "+play.price+"Kč / osoba"));
		mainLayout.addComponent(horizontalLayout);
	}
	
	private void showPrice(MyForm form, Play play){
		VerticalLayout verticalLayout = new VerticalLayout();
		verticalLayout.addComponent(new Label("Cena: "+play.price+"Kč * "+form.numberOfTickets.getValue()+"osob."));
		Integer price = play.price * (Integer)form.numberOfTickets.getValue();
		verticalLayout.addComponent(new Label("Celková cena: "+price+"Kč."));
		mainLayout.addComponent(verticalLayout);
	}
	
	private class RezervationCloseListener implements CloseListener
	{
		@Override
		public void windowClose(CloseEvent e) 
		{
			if (!readyToClose) 
			{
				Window closedWindow = e.getWindow();
				UI.getCurrent().addWindow(closedWindow);
				
				UI.getCurrent().addWindow(new ConfirmationWindow( () ->
				{
					readyToClose = true;
					close();
				})); 

			}
		}
	}
}
