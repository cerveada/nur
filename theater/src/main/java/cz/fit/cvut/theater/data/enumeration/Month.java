package cz.fit.cvut.theater.data.enumeration;

import java.util.Random;

public enum Month {
    JANUARY("Leden"),
    FEBRUARY("Únor"),
    MARCH("Březen"),
    APRIL("Duben"),
    MAY("Květen"),
    JUNE("Červen"),
    JULY("Červenec"),
    AUGUST("Srpen"),
    SEPTEMBER("Září"),
    OCTOBER("Říjen"),
    NOVEMBER("Listopad"),
    DECEMBER("Prosinec");

    private String name;

    private Month(String name) {
        this.name = name;
    }

    public static Month getRandom(){
        Random r = new Random(); // pseudonahodny generator cisel
        int count = Month.values().length;
        return Month.values()[r.nextInt(count)];
    }


    public String getName() {
        return name;
    }
}