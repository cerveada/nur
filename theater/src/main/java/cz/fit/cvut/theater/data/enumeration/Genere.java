package cz.fit.cvut.theater.data.enumeration;

public enum Genere {
	DRAMA("Činohra"), 
	OPERA("Opera"), 
	BALLET("Balet"), 
	MUSICAL("Muzikál");

	private String name;
	
	Genere(String name)
	{
		this.name = name;
	}
	
	public String getName() {
		return name;
	}
}
