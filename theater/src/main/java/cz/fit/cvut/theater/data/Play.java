package cz.fit.cvut.theater.data;

import java.sql.Date;
import java.util.ArrayList;
import java.util.GregorianCalendar;

import com.vaadin.ui.Image;
import com.vaadin.ui.Label;

import elemental.util.ArrayOfString;

public class Play {
	public String basicInfo; // name
	public String detailedInfo; // detail
	public Image image; // play photo
	public GregorianCalendar date; // show date
	public String genre; // genre (činohra, balet, opera, muzikál)
	public Actor[] actors; // actors playing in play (max 5 players)
	public String author; // author´s name
	public Integer price; // price per person
	public Play(){
		actors = new Actor [5];
	}	
}
