package cz.fit.cvut.theater.window;

import com.vaadin.data.Validator.InvalidValueException;
import com.vaadin.ui.Button;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Notification.Type;
import com.vaadin.ui.UI;
import com.vaadin.ui.Window;

import cz.fit.cvut.theater.data.MyDateFormatter;
import cz.fit.cvut.theater.data.MyForm;
import cz.fit.cvut.theater.data.Play;


public class FirstRezervationWindow extends AbstractWindow {
	
	boolean readyToClose;
	
 	public FirstRezervationWindow(Play play) // from show detail window
	{
		setCaption("Rezervace vstupenek (1/2) - zadání údajů");
	
		basicPlayInfo(play);
		MyForm form = new MyForm(null);
		mainLayout.addComponent(form);
		initFooter(form, play);
		
		addCloseListener(new RezervationCloseListener());
	}
	
	public FirstRezervationWindow(Play play, MyForm form)  // from second reservation windows
	{
		setCaption("Rezervace vstupenek (1/2) - zadání údajů");
		
		basicPlayInfo(play);
		form.setEnabled(true); // enable editing 
		mainLayout.addComponent(form);
		initFooter(form, play);
	}
		 
	private void initFooter(MyForm form, Play play)
	{
		Button rezervationButton = new Button("Pokračovat v rezervaci");
		rezervationButton.addClickListener(e ->
		{
			try 
			{
				form.validate();
				UI.getCurrent().addWindow(new SecondRezervationWindow(form, play));
				readyToClose = true;
				close();
			} 
			catch (InvalidValueException ex)
			{
				Notification.show(ex.getMessage(), Type.WARNING_MESSAGE);
			}
		});
		footer.addComponent(rezervationButton);
		
		Button backButton = new Button("Zpět");
		backButton.addClickListener(e ->
		{
			UI.getCurrent().addWindow(new ShowDetailWindow(play));
			readyToClose = true;
			close();
		});
		footer.addComponent(backButton);
	}
	
	private void basicPlayInfo(Play play){
		HorizontalLayout horizontalLayout = new HorizontalLayout();
		String date = MyDateFormatter.GetDateAndTime(play.date);
		horizontalLayout.addComponent(new Label(date));
		horizontalLayout.addComponent(new Label(", "+play.basicInfo+" ("+play.author+")"));
		horizontalLayout.addComponent(new Label(", Cena: "+play.price+"Kč / osoba"));
		mainLayout.addComponent(horizontalLayout);
	}
	
	private class RezervationCloseListener implements CloseListener
	{
		@Override
		public void windowClose(CloseEvent e) 
		{
			if (!readyToClose) 
			{
				Window closedWindow = e.getWindow();
				UI.getCurrent().addWindow(closedWindow);
				
				UI.getCurrent().addWindow(new ConfirmationWindow( () ->
				{
					readyToClose = true;
					close();
				})); 

			}
		}
	}

}
