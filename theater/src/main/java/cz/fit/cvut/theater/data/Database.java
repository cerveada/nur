package cz.fit.cvut.theater.data;

import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.List;

import com.vaadin.server.Resource;
import com.vaadin.server.ThemeResource;
import com.vaadin.ui.Image;

public class Database {
	
	public List<Play>getAllPlays()
	{
		ArrayList<Play> plays = new ArrayList<Play>();
		for (int i = 1; i < getNumberOfPlays(); i++) 
		{
			plays.add(getPlay(i));
		}
		return plays;
	}
		
	public Actor getActor(Integer actorId){
		Actor actor = new Actor();
		actor.image = getImageForActor(actorId);
		actor.name = getLabelForActor(actorId);
		actor.detailedInfo = getDetailLabelForActor(actorId);
		return actor;		
	}

	
	public Play getPlay(Integer playId){
		Play play = new Play();
		play.image = getImageForPlay(playId%5 +1);
		play.basicInfo = getDescriptionForPlay(playId%5 +1);
		play.detailedInfo = getDetailDescriptionForPlay(playId%5 +1);
		play.genre = getGenreForPlay(playId%5 +1);
		play.date = getDateForPlay(playId);
		play.actors[1] = getActor(1);
		play.actors[2] = getActor(4);
		play.actors[3] = getActor(2);
		play.author = getAuthorForPlay(playId%5 +1);
		play.price = getPriceForPlay(playId);
		return play;		
	}
	
	// PLAY
	
	public int getNumberOfPlays(){
		return 13; // plays are indexed from 1 !
	}
	
	private Image getImageForPlay(Integer playId){
		String imageName = playId + ".jpg";
		Resource res = new ThemeResource("img/plays/"+imageName);
		Image logo = new Image(null, res);
		return logo;		
	}
	
	private String getDescriptionForPlay(Integer playId){
		String string;
		switch(playId){
			case 1: string = new String("Hra o třech měsíčkách") ;
					   break;
			case 2: string = new String("Hra o čtyřech měsíčkách") ;
						break;
			case 3: string = new String("Hra o pěti měsíčkách") ;
						break;
			case 4: string =new String("Hra o šesti měsíčkách") ;
						break;
			case 5: string =new String("Hra o sedmi měsíčkách") ;
						break;
			default:   string =new String("Hra o blbečkovi, který neošetřil všechny možnosti") ;
		}
		return string;		
	}	
	
	private String getAuthorForPlay(Integer playId){
		String string;
		switch(playId){
			case 1: string = new String("Anton Pavlovič Čechov") ;
					   break;
			case 2: string = new String("Autor Autorovič") ;
						break;
			case 3: string = new String("Rejža Rejžovič") ;
						break;
			case 4: string =new String("Zvukař Zvukovič") ;
						break;
			case 5: string =new String("Kulisák Kulisovič") ;
						break;
			default:   string =new String("Nýmand Nikdovič") ;
		}
		return string;		
	}	
	
	private GregorianCalendar getDateForPlay(Integer playId){
		GregorianCalendar date;
		switch(playId){
			case 1: date =  new GregorianCalendar(2014,10,03,19,00) ;
					   break;
			case 2: date =  new GregorianCalendar(2014,10,03,21,00) ;
						break;
			case 3: date =  new GregorianCalendar(2014,10,04,19,00) ;
						break;
			case 4: date =  new GregorianCalendar(2014,10,05,18,30) ;
						break;
			case 5: date =  new GregorianCalendar(2014,10,05,20,30) ;
						break;
			case 6: date =  new GregorianCalendar(2014,10,06,17,30) ;
						break;
			case 7: date =  new GregorianCalendar(2014,11,06,20,00) ;
						break;
			case 8: date =  new GregorianCalendar(2014,10,07,19,00) ;
						break;
			case 9: date =  new GregorianCalendar(2014,10,8,19,30) ;
						break;
			case 10: date =  new GregorianCalendar(2014,11,8,21,30) ;
						break;
			case 11: date =  new GregorianCalendar(2014,11,9,19,30) ;
						break;
			case 12: date =  new GregorianCalendar(2014,11,10,20,00) ;
						break;
			case 13: date =  new GregorianCalendar(2014,11,11,18,15) ;
						break;
			default: date =  new GregorianCalendar(2014,11,31,23,59) ;
		}
		return date;		
	}
	
	private Integer getPriceForPlay(Integer playId){
		Integer price;
		switch(playId){
			case 1: price = 190;
					   break;
			case 2: price = 180;
						break;
			case 3: price = 200;
						break;
			case 4: price = 150;
						break;
			case 5: price =  250;
						break;
			case 6: price = 190;
					   break;
			case 7: price = 180;
						break;
			case 8: price = 120;
						break;
			case 9: price = 150;
						break;
			case 10: price =  250;
						break;
			case 11: price = 260;
						break;
			case 12: price = 290;
						break;
			case 13: price = 310;
						break;
			default: price =  3000;
		}
		return price;		
	}
	
	private String getGenreForPlay(Integer playId){
		String string;
		switch(playId){
			case 1: string = new String("Opera") ;
					   break;
			case 2: string = new String("Činohra") ;
						break;
			case 3: string = new String("Muzikál") ;
						break;
			case 4: string =new String("Opera") ;
						break;
			case 5: string =new String("Balet") ;
						break;
			default:   string =new String("Muzikál") ;
		}
		return string;		
	}	
	
	
	private String getDetailDescriptionForPlay(Integer playId){
		String string;
		switch(playId){
			case 1: string = new String("Ptají se často lidé. Inu jak by vypadaly - jako běžné pouťové balónky střední velikosti, tak akorát nafouknuté. Červený se vedle modrého a zeleného zdá trochu menší, ale to je nejspíš jen optický klam, a i kdyby byl skutečně o něco málo menší, tak vážně jen o trošičku. Vítr skoro nefouká a tak by se na první pohled mohlo zdát, že se balónky snad vůbec nepohybují. Jenom tak klidně levitují ve vzduchu. Jelikož slunce jasně září a na obloze byste od východu k západu hledali mráček marně, balónky působí jako jakási fata morgána uprostřed pouště.") ;
					   break;
			case 2: string = new String("Nejvíc bezpochyby zaujmou děti - jedna malá holčička zrovna včera div nebrečela, že by snad balónky mohly prasknout. A co teprve ta stuha. Stuha, kterou je každý z trojice balónků uvázán, aby se nevypustil. Očividně je uvázaná dostatečně pevně, protože balónky skutečně neucházejí. To ale není nic zvláštního. Překvapit by však mohl fakt, že nikdo, snad krom toho, kdo balónky k obloze vypustil, netuší, jakou má ona stuha barvu. Je totiž tak lesklá, že za světla se v ní odráží nebe a za tmy zase není vidět vůbec.") ;
						break;
			case 3: string = new String("Jenže kvůli všudy přítomné trávě jsou stíny balónků sotva vidět, natož aby šlo rozeznat, jakou barvu tyto stíny mají. Uvidět tak balónky náhodný kolemjdoucí, jistě by si pomyslel, že už tu takhle poletují snad tisíc let. Stále si víceméně drží výšku a ani do stran se příliš nepohybují. Proti slunci to vypadá, že se slunce pohybuje k západu rychleji než balónky, a možná to tak skutečně je. Nejeden filozof by mohl tvrdit, že balónky se sluncem závodí, ale fyzikové by to jistě vyvrátili. Z fyzikálního pohledu totiž balónky působí zcela nezajímavě.") ;
						break;
			case 4: string =new String("Stuha, kterou je každý z trojice balónků uvázán, aby se nevypustil. Očividně je uvázaná dostatečně pevně, protože balónky skutečně neucházejí. To ale není nic zvláštního. Překvapit by však mohl fakt, že nikdo, snad krom toho, kdo balónky k obloze vypustil, netuší, jakou má ona stuha barvu. Je totiž tak lesklá, že za světla se v ní odráží nebe a za tmy zase není vidět vůbec. Když svítí slunce tak silně jako nyní, tak se stuha třpytí jako kapka rosy a jen málokdo vydrží dívat se na ni přímo déle než pár chvil. Jak vlastně vypadají ony balónky?.") ;
						break;
			case 5: string =new String("To ale není nic zvláštního. Překvapit by však mohl fakt, že nikdo, snad krom toho, kdo balónky k obloze vypustil, netuší, jakou má ona stuha barvu. Je totiž tak lesklá, že za světla se v ní odráží nebe a za tmy zase není vidět vůbec. Když svítí slunce tak silně jako nyní, tak se stuha třpytí jako kapka rosy a jen málokdo vydrží dívat se na ni přímo déle než pár chvil. Jak vlastně vypadají ony balónky?. Ptají se často lidé. Inu jak by vypadaly - jako běžné pouťové balónky střední velikosti, tak akorát nafouknuté. ") ;
						break;
			default:   string =new String("Z fyzikálního pohledu totiž balónky působí zcela nezajímavě. Nejvíc bezpochyby zaujmou děti - jedna malá holčička zrovna včera div nebrečela, že by snad balónky mohly prasknout. A co teprve ta stuha. Stuha, kterou je každý z trojice balónků uvázán, aby se nevypustil. Očividně je uvázaná dostatečně pevně, protože balónky skutečně neucházejí. To ale není nic zvláštního. Překvapit by však mohl fakt, že nikdo, snad krom toho, kdo balónky k obloze vypustil, netuší, jakou má ona stuha barvu. Je totiž tak lesklá, že za světla se v ní odráží nebe a za tmy zase není vidět vůbec.") ;
		}
		return string;		
	}	

	
	
	// ACTORS
	
	public int getNumberOfActors(){
		return 7;
	}
	
	
	private Image getImageForActor(Integer num){
		String actorImageName = num + ".jpg";
		Resource res = new ThemeResource("img/actors/"+actorImageName);
		Image logo = new Image(null, res);
		return logo;		
	}
	
	private String getLabelForActor(Integer num){
		String string;
		switch(num){
			case 1: string = new String("Václav Lopata") ;
					   break;
			case 2: string = new String("Petr Krumpáč") ;
						break;
			case 3: string = new String("Kamil Kolečko") ;
						break;
			case 4: string =new String("Pavla Rýčová") ;
						break;
			case 5: string =new String("Václav Vidle") ;
						break;
			case 6: string =new String("Petr Rudlík") ;
						break;
			case 7: string =new String("Kamila Motyčková") ;
						break;
			case 8: string =new String("Vlasta Košťatová") ;
						break;
			default:string =new String("Všudbyl Hrábě") ;
		}
		return string;		
	}	
	
	private String getDetailLabelForActor(Integer num){
		String string;
		switch(num){
			case 1: string = new String("Narodil se ve Velkých senohrabech, jako malý. Lorizzle ipsum sizzle sit pimpin', consectetizzle adipiscing elit. Nullizzle uhuh ... yih! velizzle, dang volutpat, stuff quis, mah nizzle vizzle, mammasay mammasa mamma oo sa. Dope away we gonna chung. Sed eros. Mofo hizzle dolor break yo neck, yall turpizzle tempizzle things. Maurizzle fo shizzle pimpin' i saw beyonces tizzles and my pizzle went crizzle turpizzle. Cool izzle for sure. Pellentesque sure rhoncizzle shiznit. In hizzle pimpin' platea dictumst.") ;
				   break;
			case 2: string = new String("Narodil se ve Středních senohrabech, jako malý. In gravida check out this nizzle est. Boom shackalack ullamcorpizzle. Etizzle tempizzle. Donec boom shackalack mah nizzle a eros imperdizzle shit. Dizzle vel ipsum. Sizzle iaculizzle est izzle check out this. Praesent a ipsum vitae urna black ullamcorper. Integer away ass. Shut the shizzle up fo shizzle mah nizzle fo rizzle, mah home g-dizzle lacizzle vel felis.") ;
					break;
			case 3: string = new String("Narodil se v Prostředních senohrabech, jako malý. Shiz vehicula, dope laorizzle tincidunt shiznit, hizzle lorem crackalackin nunc, fo shizzle my nizzle fermentizzle things metizzle yo nibh. Duis pretizzle erat izzle fo shizzle. Phasellus accumsan. That's the shizzle get down get down natoque penatibus izzle magnizzle dizzle parturient montizzle, crazy ridiculizzle mizzle. Proin owned massa bow wow wow quam shizznit doggy. Boom shackalack non fo shizzle my nizzle.") ;
					break;
			case 4: string =new String("Narodil se v Malých senohrabech, jako malý. Integer check out this for sure. Fo shizzle sempizzle, shizzle my nizzle crocodizzle cool vehicula congue, velit erizzle convallis quizzle, shiz elementum crazy leo bling bling nulla. Vivamizzle vestibulizzle fo shizzle felizzle. Mammasay mammasa mamma oo sa rhoncus tempizzle magna. Cras interdizzle for sure leo.") ;
					break;
			case 5: string =new String("Narodil se ve Velikých senohrabech, jako malý. Nunc hizzle erizzle check it out check it out. Fo shizzle da bomb. Vestibulizzle viverra shizzle my nizzle crocodizzle shit. Quisque sizzle amet purizzle rizzle leo shizznit condimentum. Suspendisse potenti. Owned izzle hizzle vitae tincidunt ass. Sed break yo neck, yall tempizzle ante. In shiz erat et sizzle. ") ;
						break;
			case 6: string =new String("Narodil se v Zahrabaných senohrabech, jako malý. I´ts fo rizzle lacizzle fizzle sizzle. Pellentesque in fo izzle ligula stuff fizzle. Etizzle stuff pharetra dui. Aliquizzle luctizzle feugizzle break yo neck, yall. Aliquizzle gangsta volutpat. Dope sit shizzlin dizzle crunk. Break it down elementizzle felizzle izzle pimpin'. ") ;
						break;
			case 7: string =new String("Narodila se ve Vykopaných senohrabech, jako malá. Nulla izzle gangster in pizzle egestas bling bling. Integer hizzle erizzle. Fo shizzle mofo. Morbi nisi ma nizzle, auctizzle vizzle, you son of a bizzle a, cool izzle, black. Pellentesque boofron gangsta mi. Ma nizzle nisl fo shizzle, gangster quis, boofron izzle, euismizzle tellivizzle, augue. Mah nizzle leo. ") ;
						break;
			case 8: string =new String("Narodila se v Zavalených senohrabech, jako malá. Fusce gangster tortizzle da bomb erizzle ultricies funky fresh. Morbi the bizzle sodalizzle daahng dawg. Nizzle check out this, crackalackin pizzle laorizzle yo mamma, nisl velit aliquizzle mammasay mammasa mamma oo sa, vulputate that's the shizzle velizzle purizzle ut bizzle. Suspendisse tellivizzle enizzle pot check out this.") ;
						break;
			default:string =new String("Nikde se nenarodil.") ;
	}
	return string;			
	}	
}
