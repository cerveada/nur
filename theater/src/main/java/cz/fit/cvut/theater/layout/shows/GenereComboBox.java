package cz.fit.cvut.theater.layout.shows;

import java.util.ArrayList;
import java.util.List;

import cz.fit.cvut.theater.data.Play;
import cz.fit.cvut.theater.data.enumeration.Genere;
import cz.fit.cvut.theater.layout.ShowsLayout;

public class GenereComboBox extends AbstractComboBox {
	
	public GenereComboBox(ShowsLayout showsLayout) {
		super(showsLayout);

		setItemCaption(defaul, "Všechny žánry");
		
		for (Genere m : Genere.values())
		{
			addItem(m);
			setItemCaption(m, m.getName());
		}		
	}
	
	protected void onSelectionChanged()
	{
		System.out.println(getValue());

		if (getValue() instanceof Genere)
		{
			Genere selected = (Genere) getValue();
			
			List<Play> filteredPlays = new ArrayList<Play>();
			for (Play p : db.getAllPlays())
			{
				if (p.genre.equals(selected.getName()))
				{
					filteredPlays.add(p);
				}
			}
			showsLayout.setTable(new ShowTable(filteredPlays), this);
		}
		else if (getValue() instanceof DefaultItem)
		{
			showsLayout.setTable(new ShowTable(db.getAllPlays()), this);
		}
	}
}
