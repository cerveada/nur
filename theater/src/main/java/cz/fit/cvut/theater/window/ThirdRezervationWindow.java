package cz.fit.cvut.theater.window;

import com.vaadin.shared.ui.MarginInfo;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.Button;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;

import cz.fit.cvut.theater.data.MyDateFormatter;
import cz.fit.cvut.theater.data.MyForm;
import cz.fit.cvut.theater.data.Play;

public class ThirdRezervationWindow extends AbstractWindow {
	
	public ThirdRezervationWindow(MyForm form, Play play) 
	{
		setCaption("Rezervace dokončena");

		basicPlayInfo(play);
		reservationDone(form, play);

		initFooter(form, play);
	}
	
	
	
	private void basicPlayInfo(Play play){
		HorizontalLayout horizontalLayout = new HorizontalLayout();
		String date = MyDateFormatter.GetDateAndTime(play.date);
		horizontalLayout.addComponent(new Label(date));
		horizontalLayout.addComponent(new Label(", "+play.basicInfo+" ("+play.author+")"));
		horizontalLayout.addComponent(new Label(", Cena: "+play.price+"Kč / osoba"));
		mainLayout.addComponent(horizontalLayout);
	}

	private void reservationDone(MyForm form, Play play){
		VerticalLayout veticalLayout = new VerticalLayout();
		veticalLayout.setMargin(new MarginInfo(false, false, true, false));
		Integer numberOfTickets = (Integer) form.numberOfTickets.getValue();
		Label labelA = new Label(
			    "<center><h3>Vaše rezervace "+form.numberOfTickets.getValue()+" lístků proběhla úspěšně!</h3></center>",
			    ContentMode.HTML);
		if (numberOfTickets == 1){
			labelA = new Label(
				    "<center><h3>Vaše rezervace "+form.numberOfTickets.getValue()+" lístku proběhla úspěšně!</h3></center>",
				    ContentMode.HTML);
		}
		veticalLayout.addComponent(labelA);
		 Label labelB = new Label(
			    "<center><h2>Kód pro vyzvednutí lístků je \"FG4KS4RF85AH\"!</h2></center>",
			    ContentMode.HTML);
		veticalLayout.addComponent(labelB);
		Label labelD = new Label("Lístky si vyzvedněte na pokladně nejpozději 24 hodin před začátkem představení, jinak bude Vaše rezervace zrušena.");
		veticalLayout.addComponent(labelD);
		Label labelE = new Label("Rekapitulace objednávky byla zaslána na Váš email "+ form.email.getValue() + ".");
		veticalLayout.addComponent(labelE);
		if (form.mobile.getValue().length() > 0){
			Label labelF = new Label("Kód jsme Vám zaslali také v SMS na mobilní číslo "+ form.mobile.getValue() + ".");
			veticalLayout.addComponent(labelF);
		}
		Label labelG = new Label("Dotazy k rezervacím směřujte na rezervace@divadlo.cz nebo ve všední dny mezi 8 - 16 na tel. 380 254 698.");
		veticalLayout.addComponent(labelG);
		
		mainLayout.addComponent(veticalLayout);
	}
	
	
	private void initFooter(MyForm form, Play play)
	{
		Button backButton = new Button("Zavřít okno");
		backButton.addClickListener(e ->
		{
			close();
		});
		footer.addComponent(backButton);
		
		Button rezervationButton = new Button("Zpět na detail představení");
		rezervationButton.addClickListener(e ->
		{
			UI.getCurrent().addWindow(new ShowDetailWindow(play));
			close();
		});
		footer.addComponent(rezervationButton);
	}

}
