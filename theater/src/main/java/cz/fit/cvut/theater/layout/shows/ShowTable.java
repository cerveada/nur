package cz.fit.cvut.theater.layout.shows;

import java.util.List;

import com.vaadin.ui.Table;
import com.vaadin.ui.UI;

import cz.fit.cvut.theater.data.MyDateFormatter;
import cz.fit.cvut.theater.data.Play;
import cz.fit.cvut.theater.window.ShowDetailWindow;

public class ShowTable extends Table 
{

	public ShowTable(List<Play> plays)
	{
		setWidth("100%");
		
		addContainerProperty("Hra", String.class, null);
		addContainerProperty("Žánr",  String.class, null);
		addContainerProperty("Termín",  String.class, null);
		
		int i = 0;
		for (Play p : plays)
		{
			String date = MyDateFormatter.GetDateAndTime(p.date);
			addItem(new Object[]{p.basicInfo, p.genre, date}, i++);
		}
		setPageLength(size());	
		
		addItemClickListener(e -> 
		{
			ShowDetailWindow showDetailWindow = 
					new ShowDetailWindow(plays.get((int) e.getItemId()));
			UI.getCurrent().addWindow(showDetailWindow);
		});
		
		setItemDescriptionGenerator((source, itemId, propertyId) -> 
		{
			return "Detaily představení";
		});
		
	}
}
