package cz.fit.cvut.theater.layout;

import com.google.gwt.layout.client.Layout;
import com.vaadin.server.ExternalResource;
import com.vaadin.shared.ui.MarginInfo;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.BrowserFrame;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.VerticalLayout;

public class ContactLayout extends VerticalLayout {
	public ContactLayout(){
		setSpacing(true);
		setMargin(new MarginInfo(true, false, true, false));
	
		
		VerticalLayout verticalLayout = new VerticalLayout();
		verticalLayout.addComponent(labelAddresses());
		verticalLayout.addComponent(labelEmails());
		verticalLayout.addComponent(labelTelephones());
		verticalLayout.addComponent(labelTicketOffice());
		
		HorizontalLayout horizontalLayout = new HorizontalLayout();
		horizontalLayout.setSpacing(true);
		horizontalLayout.addComponent(verticalLayout);
		horizontalLayout.addComponent(browserWithMap());
		addComponent(horizontalLayout);
	}
	
	
	private Label labelAddresses(){
		Label label = new Label(
			    "Kontaktní adresa:\n" +
			    "<ul>"+
			    "  <li>Národní divadlo o.p.s.</li>"+
			    "  <li>Temelín 130</li>"+
			    "  <li>Temelín</li>"+
			    "  <li>373 01</li>"+
			    "</ul> ",
			    ContentMode.HTML);
		return label;
	}
	
	private Label labelEmails(){
		Label label = new Label(
			    "Emailové kontakty:\n"+
			    "<ul>"+
			    "  <li>informace: info@divadlo.cz</li>"+
			    "  <li>rezervace: rezervace@divadlo.cz</li>"+
			    "  <li>ředitel: reditel@divadlo.cz</li>"+
			    "</ul>" ,
			    ContentMode.HTML);
		return label;
	}
	
	private Label labelTelephones(){
		Label label = new Label(
			    "Telefonní kontakty: (po-pá 8-16)"+
			    "<ul>"+
			    "  <li>podkladna, informace: +420 380 254 698</li>"+
			    "</ul>",
			    ContentMode.HTML);
		return label;
	}
	
	private Label labelTicketOffice(){
		Label label = new Label(
			    "Otevírací doba pokladny:"+
			    "<ul>"+
			    "  <li>pondělí - pátek 8:00 - 16:00</li>"+
			    "  <li>90 minut před každým představením</li>"+
			    "</ul>",
			    ContentMode.HTML);
		return label;
	}
	
	
	private BrowserFrame browserWithMap(){
		BrowserFrame browser = new BrowserFrame("",
			    new ExternalResource("https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d2607.3679813630856!2d14.347080799999997!3d49.1935785!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x470caa08eb5afb4f%3A0x5ce234f54546effa!2sTemel%C3%ADn+130%2C+373+01+Temel%C3%ADn!5e0!3m2!1scs!2scz!4v1413841200053"));
		browser.setWidth("450px");
		browser.setHeight("430px");
		return browser;
	}

}
