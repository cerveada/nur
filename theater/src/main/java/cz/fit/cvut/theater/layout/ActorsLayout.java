package cz.fit.cvut.theater.layout;

import com.vaadin.shared.ui.MarginInfo;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.Component;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;

import cz.fit.cvut.theater.data.Actor;
import cz.fit.cvut.theater.data.Database;

public class ActorsLayout extends VerticalLayout {
	
	public ActorsLayout() 
	{
		setMargin(new MarginInfo(true, false, true, false));
		setSpacing(true);
		addStyleName("outlined");

		Database db = new Database();
		int numberOfActors = db.getNumberOfActors();
		for (int i = 1; i <= numberOfActors; i++) 
		{
			Actor actor = db.getActor(i);

			HorizontalLayout actorMainLayout = new HorizontalLayout();
			actorMainLayout.setWidth("100%");
			actorMainLayout.setSpacing(true);
			actor.image.setHeight("200px");
			actorMainLayout.addComponent(actor.image);

			VerticalLayout vertLayout = new VerticalLayout();
			vertLayout.setSpacing(true);
			vertLayout.addComponent(new Label( "<strong>"+actor.name+"</strong>",
						    ContentMode.HTML));
			Label label = new Label(actor.detailedInfo);
			vertLayout.addComponent(label);
			actorMainLayout.addComponent(vertLayout);
			actorMainLayout.setExpandRatio(vertLayout, 5);


			addComponent(actorMainLayout);
		}
	}
}
