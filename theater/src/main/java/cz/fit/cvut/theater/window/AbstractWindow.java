package cz.fit.cvut.theater.window;

import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;

public abstract class AbstractWindow extends Window {
	
	private VerticalLayout content;
	VerticalLayout mainLayout;
	HorizontalLayout footer;

	
	public AbstractWindow() 
	{
		center();
		setResizable(false);
		setWidth("700px");
		setHeight("700px");
		setModal(true);
		
		content = new VerticalLayout();
		content.setSizeFull();
		content.setMargin(true);
		content.setSpacing(true);
		setContent(content);
		
		mainLayout = new VerticalLayout();
		mainLayout.setSpacing(true);
		content.addComponent(mainLayout);
		content.setExpandRatio(mainLayout, 2);
		
		footer = new HorizontalLayout();
		footer.setSpacing(true);
		content.addComponent(footer);
	}

}
