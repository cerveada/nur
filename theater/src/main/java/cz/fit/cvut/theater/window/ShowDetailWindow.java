package cz.fit.cvut.theater.window;

import com.vaadin.shared.ui.MarginInfo;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Component;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Layout;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;

import cz.fit.cvut.theater.MainLayout;
import cz.fit.cvut.theater.data.Database;
import cz.fit.cvut.theater.data.MyDateFormatter;
import cz.fit.cvut.theater.data.MyForm;
import cz.fit.cvut.theater.data.Play;

public class ShowDetailWindow extends AbstractWindow {
		
	public ShowDetailWindow(Play play) 
	{
		setCaption("Detaily Představení");
		
		createComponentFromPlay(play);
		initFooter(play);
	}
	
	private void createComponentFromPlay(Play play){
		VerticalLayout vl = new VerticalLayout();
		Label name = new Label(play.basicInfo + " ("+play.genre+")");
		Label date = new Label(MyDateFormatter.GetDateAndTime(play.date));
		vl.addComponent(name);
		vl.addComponent(date);
		vl.addComponent(new Label(play.author));
		vl.addComponent(new Label("Cena: "+play.price+"Kč / osoba"));
		vl.setWidth("200px");
		
		HorizontalLayout horizontalLayout = new HorizontalLayout();
		horizontalLayout.setSizeUndefined();
		horizontalLayout.addComponent(vl);
		play.image.setWidth("480px");
		horizontalLayout.addComponent(play.image);
		
		mainLayout.addComponent(horizontalLayout);
		mainLayout.addComponent(new Label(play.detailedInfo));
	}
	
	private void initFooter(Play play)
	{			
		Button rezervationButton = new Button("Rezervovat vstupenky");
		rezervationButton.addClickListener(e ->
		{
			UI.getCurrent().addWindow(new FirstRezervationWindow(play));
			close();
		});
		footer.addComponent(rezervationButton);
	}

}
